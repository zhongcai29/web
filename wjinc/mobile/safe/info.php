<?php $this->display('inc_header.php'); ?>
<?php $this->display('modal.php'); ?>

<style>
    .table{
        font-size: 14px;
        width: 95%;
        margin: 0 auto;
    }
    .table  .title{
        font-weight: bold;
    }
    .h4{
        margin-left: 20px;
        font-weight: bold;
        font-size: 14px;
        margin-top: 20px;
        font-family: "微软雅黑", "Microsoft Sans Serif";
    }
    .tips p{
        margin-left: 20px;
        font-size: 14px;
    }
    button{
        font-family: '微软雅黑',Tahoma,Consolas,Arial,Simsun;
    }
    input, select{
        width:80%;
        height: 25px;
        border-radius: 5px;
    }
</style>
<h4 class="h4">个人基本信息</h4>
<table class="table table-striped">
    <tr>
        <td class="title">登陆账号：</td>
        <td align="left"><?=$this->user['username']?></td>
        <td class="title">会员昵称：</td>
        <td align="left"><?=$this->user['nickname']?></td>
    </tr>
    <tr>
        <td class="title">可用资金：</td>
        <td align="left"><?=$this->user['coin']?> 元</td>
        <td class="title">积分：</td>
        <td align="left"><?=$this->user['score']?> 元</td>
    </tr>
    <tr>
        <td class="title">会员类型：</td>
        <td align="left"><?=$this->iff($this->user['type'], '代理', '会员')?></td>
        <td class="title">VIP等级：</td>
        <td align="left">VIP<?=$this->user['grade']?></td>
    </tr>
</table>

<?php
$bank_lists = $this->getRows("select * from {$this->prename}bank_list  where  isDelete=0 AND type=1");
$myBank = $this->getRow("select * from {$this->prename}member_bank where uid=? limit 1", $this->user['uid']); if($this->user['coinPassword']){ ?>

<form action="/index.php/safe/setCBAccount" method="post" target="ajax" onajax="safeBeforSetCBA" call="safeSetCBA" name="form1">
    <table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
        <tr class='table_b_th'>
            <td align="left" style="font-weight:bold;padding-left:10px;" colspan=4>个人账户信息</td>
        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" width="15%" style="font-weight:bold;">选择银行：</td>
            <td align="left" width="40%">
                <select name="bankname">
					<?php
					foreach($bank_lists as $var){
						?>
                        <option value="<?=$var['sortname']?>" <?=(isset($myBank)&&$myBank['bankname'] == $var['sortname']?'selected="selected"':'') ?>><?=$var['sortname']?></option>
					<?php }?>
                </select>
            </td>
        </tr>

        <tr height=25 class='table_b_tr_b'>
            <td align="right" width="15%" style="font-weight:bold;">银行账号：</td>
            <td align="left" width="40%">
                <input type="text" name="bankId" value="<?=$myBank['bankId']?>" <?/*=$this->iff($myBank['bankId'], 'readonly')*/?>/>
            </td>
        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" width="12%" style="font-weight:bold;">银行姓名：</td>
            <td align="left" ><input type="text" name="username" value="<?=$myBank['username']?>" <?/*=$this->iff($myBank['username'], 'readonly')*/?> />
            </td>
        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" style="font-weight:bold;">支付宝账号：</td>
            <td align="left" ><input type="text" name="account" value="<?=$myBank['account']?>" placeholder="若已填银行卡信息则选填" <?/*=$this->iff($myBank['account'], 'readonly')*/?>/></td>
        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" style="font-weight:bold;">支付宝姓名：</td>
            <td align="left" ><input type="text"  name="countname" value="<?=$myBank['countname']?>"   <?/*=$this->iff($myBank['countname'], 'readonly')*/?> /></td>
        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" style="font-weight:bold;">资金密码：</td>
            <td align="left" ><input type="password" name="coinPassword"/></td>

        </tr>
        <tr height=25 class='table_b_tr_b'>
            <td align="right" style="font-weight:bold;"></td>
            <td align="left">
                <button type="submit"  <?=$this->iff($flag, 'disabled')?>   class="btn">保存信息</button>
                <button type="reset"  onClick="this.form.reset()"  class="btn">重置</button>
            </td>
        </tr>
    </table>
</form>
        <?php
}else{
		?>
        <div class="tips">
            <p>温馨提示：</p>
            <p>设置银行信息要用资金密码，您尚未设置资金密码！</p>
            <p><a href="/index.php/safe/passwd" style="text-decoration:none; color:#f00">设置资金密码>></a></p>
        </div>

<?php }?>

<script>
    function safeBeforSetCBA(){
        if(!this.bankId.value && !this.account.value){winjinAlert("银行帐号没有填写","alert");return false;}
        if(this.bankId.value && !this.username.value){winjinAlert("银行开户名没有填写","alert");return false;}
        if(this.account.value && !this.countname.value){winjinAlert("支付宝名没有填写","alert");return false;}
        if(!this.coinPassword.value){winjinAlert("请输入资金密码","alert");return false;}
        if(this.coinPassword.value<6){winjinAlert("资金密码至少6位","alert");return false;}
        return true;
    }
    function safeSetCBA(err, data){
        if(err){
            winjinAlert(err,"err");

        }else{
            winjinAlert(data,"ok");
            location.reload();
        }
    }
</script>
<?php $this->display('inc_footer.php'); ?> 

  
   
 