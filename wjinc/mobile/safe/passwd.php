

<?php $this->display('inc_header.php'); ?>
<?php $this->display('modal.php'); ?>
<style>
    .pagemain{
        width: 100%;
    }
    .maindiv{
        width: 95%;
        margin: 0 auto;
    }
    h3{
        font-weight: bold;
        font-size: 16px;
        margin-top: 10px;
        margin-bottom: 10px;
        font-family: "微软雅黑", "Microsoft Sans Serif";
    }
    .wjform{
        height: 40px;
        width: 100%;
    }
    .wjform dt{
        float: left;
        width: 28%;
    }
    .wjform input{
        width: 60%;
    }
    .pagemain input.btn{
        padding-left: 10px;
    }
</style>

    <div class="pagemain">
    	<div class="maindiv">
            <form action="/index.php/safe/setPasswd" method="post" target="ajax" onajax="safeBeforSetPwd" call="safeSetPwd">
                <h3>登录密码管理</h3>
                <dl class="wjform">
                    <dt>原始密码：</dt>
                    <dd><input type="password" name="oldpassword" /></dd>
                </dl>
                <dl class="wjform">
                    <dt>新密码：</dt>
                    <dd><input type="password" name="newpassword" /></dd>
                </dl>
                <dl class="wjform">
                    <dt>确认密码：</dt>
                    <dd><input type="password"   class="confirm"/></dd>
                </dl>
                <dl class="wjform">
                    <dt>&nbsp;</dt>
                    <dd><input type="button" id='put_button_pass' class="btn" value="修改密码" onclick="$(this).closest('form').submit()" >
                        <input type="reset" value="重置" onClick="this.form.reset()"  class="btn"/></dd>
                </dl>
            </form>

			<?php if($args[0]){ ?>

                <form action="/index.php/safe/setCoinPwd2" method="post" target="ajax" onajax="safeBeforSetCoinPwd2" call="safeSetPwd">
                    <h3 class="mt10">资金密码管理</h3>
                    <div class="tips">
                        <dl>
                            <dt>温馨提示：</dt>
                            <dd>资金密码：提款、充值、还有积分兑换等都要求必须输入资金密码！</dd>
                        </dl>
                    </div>
                    <dl class="wjform">
                        <dt>原始密码：</dt>
                        <dd><input type="password" name="oldpassword"  /></dd>
                    </dl>
                    <dl class="wjform">
                        <dt>新密码：</dt>
                        <dd><input type="password" name="newpassword"  /></dd>
                    </dl>
                    <dl class="wjform">
                        <dt>确认密码：</dt>
                        <dd><input type="password" class="confirm"  /></dd>
                    </dl>
                    <dl class="wjform">
                        <dt>&nbsp;</dt>
                        <dd><input type="button" id='put_button_pass' class="btn" value="设置密码"  onclick="$(this).closest('form').submit()">
                            <input type="reset" value="重置" onClick="this.form.reset()"  class="btn"/></dd>
                    </dl>
                </form>

			<?php }else{?>
                <form action="/index.php/safe/setCoinPwd" method="post" target="ajax" onajax="safeBeforSetCoinPwd" call="safeSetPwd">
                    <h3 class="mt10">设置资金密码</h3>
                    <div class="tips">
                        <dl>
                            <dt>温馨提示：</dt>
                            <dd>资金密码：提款、充值、还有积分兑换等都要求必须输入资金密码！</dd>
                        </dl>
                    </div>
                    <dl class="wjform">
                        <dt>密码：</dt>
                        <dd><input type="password" name="newpassword"  /></dd>
                    </dl>
                    <dl class="wjform">
                        <dt>确认密码：</dt>
                        <dd><input type="password" class="confirm"  /></dd>
                    </dl>
                    <dl class="wjform">
                        <dt>&nbsp;</dt>
                        <dd><input type="button" id='put_button_pass' class="btn" value="设置密码"  onclick="$(this).closest('form').submit()">
                            <input type="reset" value="重置" onClick="this.form.reset()"  class="btn"/></dd>
                    </dl>
                </form>

			<?php }?>
        </div>
    </div>
<script>
    function safeSetPwd(err, data){
        if(err){
            winjinAlert(err,"err");
        }else{
            this.reset();
            winjinAlert(data,"ok");
            setTimeout(function () {
                location.href = '/safe/info';
            }, 1000)
        }
    }
</script>
<?php $this->display('inc_footer.php'); ?> 

  
   
 