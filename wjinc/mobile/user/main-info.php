<?php $this->display('inc_header.php') ?>

<style type="text/css">
	.user_action{
		border-top: 1px solid #ccc;
		border-bottom: 1px solid #ccc;
	}
	.user_action:nth-child(odd){
		margin-bottom: 40px;
	}
	.actions{
		display: block;
		padding: 10px;
		border-bottom: 1px solid #ccc;
		font-size: 16px;
	}
	.user_action .actions a{
		margin-left: 10px;
	}
	.user_action .actions a span{
		padding-left: 64%;
	}
</style>

<ul class="user_action">
    <li class="actions glyphicon glyphicon-list-alt"><a href="/record/search">投注记录<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
	<li class="actions glyphicon glyphicon-th"><a href="/cash/rechargeLog">充值记录<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
	<li class="actions glyphicon glyphicon-tasks"><a href="/cash/toCashLog">提现记录<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
	<li class="actions glyphicon glyphicon-equalizer"><a href="/report/coin">资金明细<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
</ul>
<ul class="user_action">
	<li class="actions glyphicon glyphicon-lock"><a href="/safe/passwd">修改密码<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
	<li class="actions glyphicon glyphicon-credit-card"><a href="/safe/info">修改银行<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></a></li>
</ul>


<?php $this->display('inc_footer.php') ?>