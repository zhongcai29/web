<table width="100%" class='table_b'>
    <thead>
    <thead>
    <tr class="table_b_th">
        <td>充值金额</td>
        <td>实际到账</td>
        <td>状态</td>
        <td>充值时间</td>
        <td>备注</td>
    </tr>
    </thead>
    <tbody class="table_b_tr">
    <?php
    $sql="select a.* from {$this->prename}member_recharge a  where a.uid={$this->user['uid']} and a.isDelete=0";

    $sql.=' order by a.id desc';
    $this->pageSize=12;
    $list=$this->getPage($sql, $this->page, $this->pageSize);

    if($list['data']) foreach($list['data'] as $var){
        ?>
        <tr>
            <td><?=$var['amount']?></td>
            <td><?=$this->iff($var['rechargeAmount']>0, $var['rechargeAmount'], '--')?></td>
            <td><?=$this->iff($var['state'], '充值成功', '<span style="color:#653809">正在处理</span>')?></td>
            <td><?=$this->iff($var['state'], date('m-d H:i', $var['actionTime']), '--')?></td>
            <td><?=$var['info']?></td>
        </tr>
    <?php }else{ ?>
        <tr>
            <td colspan="7" align="center">没有充值记录</td>
        </tr>
    <?php } ?>
    </tbody>

</table>
<?php
$this->display('inc_page.php', 0, $list['total'], $this->pageSize, "/index.php/cash/rechargeList-{page}");
?>
<script>
    $(function(){
        $('.bottompage a').click( function(){
            $('#biao-cont').load($(this).attr('href'));
            return false;
        });
    });
</script>




