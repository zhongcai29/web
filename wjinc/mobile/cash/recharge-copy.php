<!--//复制程序 flash+js------end-->

<?php
$mBankId=$args[0]['mBankId'];
$sql="select mb.*, b.name bankName, b.logo bankLogo, b.home bankHome from {$this->prename}sysadmin_bank mb, {$this->prename}bank_list b where mb.id=$mBankId and b.isDelete=0 and mb.bankId=b.id";
$memberBank=$this->getRow($sql);
if($memberBank['bankId']==2){
	?>

    <table width="100%" border="0" cellspacing="1" cellpadding="3" class='table_b'>
        <tr class='table_b_th'>
            <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>充值信息</td>
        </tr>

        <tr height=25 class='table_b_tr_b' >
            <td align="right" class="copys">扫码充值：</td>
            <td align="left" ><img id="bank-type-icon" src="<?=$memberBank['qr_code']?>" title="<?=$memberBank['bankName']?>" />
            </td>
        </tr>
        <div class="tips" style="margin: 10px 20px;font-size: 14px" >
            <dl>
                <dt>充值说明：</dt>
                <dd>1.<?= $this->settings['webName']?>已与支付宝合作，开通直接扫码充值功能。</dd>
                <dd>2.转账后如5分钟未到账，请联系客服，告知您的充值金额及你的支付宝名。</dd>
            </dl>
        </div>
    </table>
<? }else{  //其它收款方式 ?>
    <!--左边栏body-->
    <table width="100%" border="0" cellspacing="1" cellpadding="4" class='table_b'>
        <tr class='table_b_th'>
            <td align="left" style="font-weight:bold;padding-left:10px;" colspan=2>充值信息</td>
        </tr>

        <tr height=25 class='table_b_tr_b' >
            <td align="right" width="20%" class="copys">扫码充值：</td>
            <td align="left" width="80%" ><img id="bank-type-icon" src="<?=$memberBank['qr_code']?>" title="<?=$memberBank['bankName']?>" />
            </td>
        </tr>
        <tr>
            <div class="tips" style="margin: 10px 20px;font-size: 14px" >
                <dt>充值说明：</dt>
                <dd>1.<?=$this->settings['webName']?>已与微信合作，开通直接扫码充值功能。</dd>
                <dd>2.转账后如5分钟未到账，请联系客服，告知您的充值金额及你的微信名。</dd>
            </div>
        </tr>

    </table>
<?php }?>
<a href="/index/cooperateHall" style="color: #f33;font-size: 18px;font-family: '微软雅黑', 'Microsoft Sans Serif';margin: 10px 0 0 60px;">充值成功，立即购彩 ></a>
