<table width="100%" class='table_b'>
	<thead>
	<thead>
	<tr class="table_b_th">
		<td>申请时间</td>
		<td>提现金额</td>

		<td>提现银行</td>
		<td>银行尾号</td>
		<td>状态</td>
	</tr>
	</thead>
	<tbody class="table_b_tr">
	<?php
	$sql="select * from {$this->prename}member_cash  where uid={$this->user['uid']} and isDelete=0";

	$this->pageSize = 12;
	$stateName=array('已到帐', '正在办理', '已取消', '已支付', '失败');

	$list=$this->getPage($sql, $this->page, $this->pageSize);
	if($list['data']) foreach($list['data'] as $var){
		?>
		<tr>
			<td><?=date('m-d H:i', $var['actionTime'])?></td>
			<td><?=$var['amount']?></td>
			<td><?=$var['bankname']?></td>
			<td><?=preg_replace('/^.*(.{4})$/', "$1", $var['account'])?></td>
			<td>
				<?php
				if($var['state']==3){
					echo '<div class="sure" id="', $var['id'], '"></div>';
				}else if($var['state']==4){
					echo '<span title="'.$var['info'].'" style="cursor:pointer; color:#f00;">'.$stateName[$var['state']].'</span>';
				}else{
					echo $stateName[$var['state']];
				}
				?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<?php
$this->display('inc_page.php',0,$list['total'],$this->pageSize, "/index.php/{$this->controller}/cashLog-{page}");
?>
<script>
    $(function(){
        $('.bottompage a').click( function(){
            $('#biao-cont').load($(this).attr('href'));
            return false;
        });
    });
</script>


