<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://apps.bdimg.com/libs/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://apps.bdimg.com/libs/jquery/1.8.3/jquery.min.js"></script>
    <link rel="stylesheet" href="/skin/main/bl_ococ.css">
    <script src="/skin/main/onload.js"></script>
</head>
<style>
    body{
        background-color: #f5f7fa;
    }
    .login .btn {
        width: 100%;
    }
    .login {
        width: 90%;
        margin: 0 auto;
    }
    .nav-header{
        line-height: 50px;
        height: 50px;
        margin-bottom: 15px;
    }
    .nav-header li{
        list-style-type: none;
        float: left;
        background-color: #E9573F;
        text-align: center;
    }
    .nav-header a{
        font-size: 16px;
        color: #FFce54;
        font-weight: bold;
    }
</style>
<body>




<ul class="nav-header">
    <li class="active" style="width: 19%"><a href="/">首页</a></li>
    <li style="width: 27%" ><a href="/index/purchaseHall">购彩大厅</a></li>
    <li style="width: 27%"><a href="/index/cooperateHall">合买大厅</a></li>
    <!--<li ><a href="/about">关于我们</a></li>-->
    <?php if($_SESSION[$this->memberSessionName]){ ?>
       <li style="width: 27%"><a href="/index/mainInfo">个人中心</a></li>
    <?php } else { ?>
        <li style="width: 27%"><a href="/user/login">登录</a></li>
    <?php } ?>
</ul>



<?php
if($_SESSION[$this->memberSessionName]){
$ngrade=$this->getValue("select max(level) from {$this->prename}member_level where minScore <= {$this->user['scoreTotal']} limit 1");
if($ngrade>$this->user['grade']){
    $sql="update ssc_members set grade={$ngrade} where uid=?";
    $this->update($sql, $this->user['uid']);
}else{$ngrade=$this->user['grade'];}
$date=strtotime('00:00:00');
$this->freshSession();
?>
<style type="text/css">
    #userclass{
        font-size:15px; margin: -5px auto 10px auto;
    }
    #userclass span{margin: auto 2px auto 2px;}
</style>
<div id="userclass">
    <span>用户： <a href="/user/mainInfo"><span class="fee"><?=$this->user['username']?></a></span></span>
    <span>余额：<strong>&yen;<span  class="green"><?=$this->user['coin']?></span></strong>
    <span><a href="/cash/recharge" style="color: #E9573F;font-weight: bold">充值</a></span>
    <span><a href="/cash/toCash" class="green" style="font-weight: bold">提款</a></span>
</div>
<? }?>