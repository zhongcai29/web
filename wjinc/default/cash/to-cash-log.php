<?php $this->display('inc_left.php') ?>
<script type="text/javascript">
$(function(){
	$('.sure[id]').click(function(){
		var $this=$(this),
		cashId=$this.attr('id'),
		
		call=function(err, data){
			if(err){
				alert(err);
			}else{
				this.parent().text('已到帐');
			}
		}
		
		$.ajax('/index.php/cash/toCashSure/'+cashId,{
			dataType:'json',
			
			error:function(xhr, textStatus, errThrow){
				call.call($this, errThrow||textStatus);
			},
			
			success:function(data, textStatus, xhr){
				var errorMessage=xhr.getResponseHeader('X-Error-Message');
				if(errorMessage){
					call.call($this, decodeURIComponent(errorMessage), data);
				}else{
					call.call($this, null, data);
				}
			}
		});
	});
});
</script>
<div class="pagemain">
    <div class="search">
        时间：<input type="text" name="fromTime" class="datainput"  value="<?=date('Y-m-d', strtotime('-1 year'))?>"/>至<input type="text" name="toTime"  class="datainput" value="<?=date('Y-m-d')?>"/>
    </div>
    <div class="display biao-cont">
        <!--下注列表-->
        <table width="100%" class='table_b'>
        <thead>
            <thead>
            <tr class="table_b_th">
                <td>提现金额</td>
                <td>申请时间</td>
                <td>提现银行</td>
                <td>银行尾号</td>
                <td>状态</td>
            </tr>
            </thead>
            <tbody class="table_b_tr">
            <?php
                $sql="select * from {$this->prename}member_cash  where   uid={$this->user['uid']} and  isDelete=0";
                $stateName=array('已到帐', '正在办理', '已取消', '已支付', '失败');
                $list=$this->getPage($sql, $this->page, $this->pageSize);
                if($list['data']) foreach($list['data'] as $var){
            ?>
            <tr>
                <td><?=$var['amount']?></td>
                <td><?=date('Y-m-d H:i:s', $var['actionTime'])?></td>
                <td><?=$var['bankname']?></td>
                <td><?=preg_replace('/^.*(.{4})$/', "$1", $var['account'])?></td>
                <td>
                <?php
                    if($var['state']==3){
                        echo '<div class="sure" id="', $var['id'], '"></div>';
                    }else if($var['state']==4){
                        echo '<span title="'.$var['info'].'" style="cursor:pointer; color:#f00;">'.$stateName[$var['state']].'</span>';
                    }else{
                        echo $stateName[$var['state']];
                    }
                ?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php
            $this->display('inc_page.php', 0, $list['total'], $this->pageSize, "/index.php/cash/toCashLog-{page}?fromTime={$_GET['fromTime']}&endTime={$_GET['endTime']}");
        ?>
        <!--下注列表 end -->
    </div>
	
</div>
<!--以下为模板代码-->
<?php $this->display('inc_footer.php') ?>
 <script type="text/javascript">
    $("#membernav").show();
 </script>
   
 