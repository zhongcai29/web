<?php
@session_start();
class Safe extends WebLoginBase{
	public $title='';
	private $vcodeSessionName='ssc_vcode_session_name';
	/**
	 * 用户信息页面
	 */
	public final function info(){
		$this->display('safe/info.php');
	}
	/**
	 * 密码管理
	 */
	public final function passwd(){
		$sql="select password, coinPassword from {$this->prename}members where uid=?";
		$pwd=$this->getRow($sql, $this->user['uid']);
		if(!$pwd['coinPassword']){
			$coinPassword=false;
		}else{
			$coinPassword=true;
		}

		$this->display('safe/passwd.php',0,$coinPassword);
	}
	
	/**
	 * 设置密码
	 */
	public final function setPasswd(){
		$opwd=$_POST['oldpassword'];
		if(!$opwd) throw new Exception('原密码不能为空');
		if(strlen($opwd)<6) throw new Exception('原密码至少6位');
		if(!$npwd=$_POST['newpassword']) throw new Exception('密码不能为空');
		if(strlen($npwd)<6) throw new Exception('密码至少6位');
		
		$sql="select password from {$this->prename}members where uid=?";
		$pwd=$this->getValue($sql, $this->user['uid']);

		if(!password_verify($opwd, $pwd)) throw new Exception('原密码不正确');
		
		$sql="update {$this->prename}members set password=? where uid={$this->user['uid']}";
		if($this->update($sql, password_hash($npwd, PASSWORD_BCRYPT))) return '修改密码成功';
		return '修改密码失败';
	}
	
	/**
	 * 设置资金密码
	 */
	public final function setCoinPwd(){
		$opwd=$_POST['oldpassword'];
		if(!$npwd=$_POST['newpassword']) throw new Exception('资金密码不能为空');
		if(strlen($npwd)<6) throw new Exception('资金密码至少6位');
		
		$sql="select password, coinPassword from {$this->prename}members where uid=?";
		$pwd=$this->getRow($sql, $this->user['uid']);
		if(!$pwd['coinPassword']){
			$npwd=md5($npwd);
			if($npwd==$pwd['password']) throw new Exception('资金密码与登录密码不能一样');
			$tishi='资金密码设置成功';
		}else{
			if($opwd && md5($opwd)!=$pwd['coinPassword']) throw new Exception('原资金密码不正确');
			$npwd=md5($npwd);
			if($npwd==$pwd['password']) throw new Exception('资金密码与登录密码不能一样');
			$tishi='修改资金密码成功';
		}
		$sql="update {$this->prename}members set coinPassword=? where uid={$this->user['uid']}";
		if($this->update($sql, $npwd)) return $tishi;
		return '修改资金密码失败';
	}
	
	public final function setCoinPwd2(){
		$opwd=$_POST['oldpassword'];
		if(!$opwd) throw new Exception('原资金密码不能为空');
		if(strlen($opwd)<6) throw new Exception('原资金密码至少6位');
		if(!$npwd=$_POST['newpassword']) throw new Exception('资金密码不能为空');
		if(strlen($npwd)<6) throw new Exception('资金密码至少6位');
		
		$sql="select password, coinPassword from {$this->prename}members where uid=?";
		$pwd=$this->getRow($sql, $this->user['uid']);
		if(!$pwd['coinPassword']){
			$npwd=md5($npwd);
			if($npwd==$pwd['password']) throw new Exception('资金密码与登录密码不能一样');
			$tishi='资金密码设置成功';
		}else{
			if($opwd && md5($opwd)!=$pwd['coinPassword']) throw new Exception('原资金密码不正确');
			$npwd=md5($npwd);
			if($npwd==$pwd['password']) throw new Exception('资金密码与登录密码不能一样');
			$tishi='修改资金密码成功';
		}
		$sql="update {$this->prename}members set coinPassword=? where uid={$this->user['uid']}";
		if($this->update($sql, $npwd)) return $tishi;
		return '修改资金密码失败';
	}
	
	/**
	 * 设置银行帐户
	 */
	public final function setCBAccount(){
		if(!$_POST) throw new Exception('参数出错');

		$update['account']=wjStrFilter($_POST['account']);
		$update['bankname']=wjStrFilter($_POST['bankname']);
		$update['countname']=wjStrFilter($_POST['countname']);
		$update['username']=wjStrFilter($_POST['username']);
		$update['bankId']=wjStrFilter($_POST['bankId']);
		$update['coinPassword']=$_POST['coinPassword'];

		if(!isset($update['bankId'])) throw new Exception('请选择银行账号!');
		if(!isset($update['username'])) throw new Exception('请填写银行账户名!');
		if(!isset($update['account'])) throw new Exception('请填写支付宝账号!');
		if(!isset($update['countname'])) throw new Exception('请填写支付宝姓名!');

		$a=strlen($update['username']);
		$b=mb_strlen($update['username'],'utf8');
		if(($a!=$b && $a%$b==0)==FALSE) throw new Exception('银行账户名必须为汉字');
		unset($a);unset($b);

		// 更新用户信息缓存
		$this->freshSession();
		if(md5($update['coinPassword'])!=$this->user['coinPassword']) throw new Exception('资金密码不正确');
		unset($update['coinPassword']);
		$update['uid']=$this->user['uid'];
		$update['editEnable']=0;//设置过银行
		

		if($bank=$this->getRow("select editEnable from {$this->prename}member_bank where uid=? LIMIT 1", $this->user['uid'])){
			$update['xgtime']=$this->time;
			//if($bank['editEnable']!=1) throw new Exception('银行信息绑定后不能随便更改，如需更改，请联系在线客服');
			if($this->updateRows($this->prename .'member_bank', $update, 'uid='. $this->user['uid'])){
				return '更改账户信息成功';
			}else{
				throw new Exception( '更改银行信息出错');
			}
		}else{
			//检查银行账号唯一
			//if($account=$this->getValue("select account FROM {$this->prename}member_bank where account=? LIMIT 1",$update['account'])) throw new Exception('该'.$account.'支付宝号已经使用');
			//检查账户名唯一
			//if($bankId=$this->getValue("select bankId FROM {$this->prename}member_bank where bankId=? LIMIT 1",$update['bankId'])) throw new Exception('该'.$bankId.'银行账号已经使用');
			$update['bdtime']=$this->time;
			if($this->insertRow($this->prename .'member_bank', $update)){
				$this->getSystemSettings();
				if($coin=floatval($this->settings['huoDongRegister'])){
					$liqType=51;
					$info='首次绑定收款赠送';
					$ip=$this->ip(true);
					$bankAccount=$update['account'];
					// 查找是否已经赠送过
					$sql="select id from {$this->prename}coin_log where liqType=$liqType and (`uid`={$this->user['uid']} or extfield0=$ip or extfield1='$bankAccount') limit 1";
					if(!$this->getValue($sql)){
						$this->addCoin(array(
							'coin'=>$coin,
							'liqType'=>$liqType,
							'info'=>$info,
							'extfield0'=>$ip,
							'extfield1'=>$bankAccount
						));
						return sprintf('绑定银行信息成功，系统赠送%.2f元', $coin);
					}
				}

				return '绑定收款方式成功！';
			}else{
				throw new Exception( '更改银行信息出错！');
			}
		}
	}

//设置登陆问候语
public final function care(){
		if(!$_POST) throw new Exception('提交参数出错');

		//过滤未知字段
		$update['care']=wjStrFilter($_POST['care']);

        //问候语长度限制
		$len=mb_strlen($update['care'],'utf8');
		if($len>10) throw new Exception( '登陆问候语过长，请重新输入');
		if($len=0) throw new Exception( '登陆问候语不能为空，请重新输入');

		if($this->updateRows($this->prename .'members', $update, 'uid='. $this->user['uid'])){
				return '更改登陆问候语成功';
			}else{
				throw new Exception( '更改登陆问候语出错');
			}
  }

 //设置昵称
public final function nickname(){
		if(!$_POST) throw new Exception('提交参数出错');

		//过滤未知字段
		$update['nickname']=wjStrFilter($_POST['nickname']);

		$len=mb_strlen($update['nickname'],'utf8');
		if($len>8) throw new Exception( '昵称过长，请重新输入');
		if($len=0) throw new Exception( '昵称不能为空，请重新输入');

		if($this->updateRows($this->prename .'members', $update, 'uid='. $this->user['uid'])){
				return '更改昵称成功';
			}else{
				throw new Exception( '更改昵称出错');
			}
  }
}